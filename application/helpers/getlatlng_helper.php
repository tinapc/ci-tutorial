<?php

/* 
 * This function using to get Lat and Lng from a address via google map api
 */

if(!function_exists('get_latlng')) {
    function get_latlng($address) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";

        //Lay du lieu tra ve bang cach su dung ham file_get_contents
        $getmap = file_get_contents($url);

        //Ket qua tra ve la dang json, To decode, su dung ham json_decode()
        $googlemap = json_decode($getmap);
        
        foreach($googlemap->results as $res){
            $address = $res->geometry;
            $latlng = $address->location;
            //$formattedaddress = $res->formatted_address;
            //echo "Latitude: ". $latlng->lat ."<br />". "Longitude:". $latlng->lng;
        }
        
        //tra ve mot mang voi gia tri cua lat + lng
        return $latlng;
    }
}


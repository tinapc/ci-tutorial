<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script>

        function initialize() {
          var myLatlng = new google.maps.LatLng('<?=$mylatlng->lat?>', '<?=$mylatlng->lng?>');
          var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }

          var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

          var contentString = '<h3><?=$title?></h3>';
              contentString +='<p>Địa chỉ: <?=$address?></p>';
              contentString +='<p>Điện thoại: <?=$phone?></p>';
              contentString +='<p>Email: <?=$email?></p>';
              
          var infowindow = new google.maps.InfoWindow({
              content: contentString
          });

          var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title: '<?=$title?>'
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
          });
        }

        google.maps.event.addDomListener(window, 'load', initialize);

            </script>
    </head>
    <body>
        <div id="map-canvas" style="width:900px; height: 500px"></div>
    </body>
</html>
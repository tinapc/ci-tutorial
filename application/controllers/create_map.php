<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create_map extends CI_Controller {

	public function __construct(){
		parent::__construct();

		//Load helper getlatlang_helper
		$this->load->helper('getlatlng');
	}

	public function index()
	{
		//Toi su dung dia chi nhu sau: 234 Quang Trung, Quảng Ngãi
		$ex_address = "234 Quang Trung, Quảng Ngãi";

		//Lay gia tri lat + lng tu dia chi tren
		$my_latlng = get_latlng(urlencode($ex_address));

		//Tao mot so thong tin lien lac de show khi user click len marker
		$title = 'Địa chỉ của tôi';
		$address = $ex_address;
		$phone = '0909-000-000';
		$email = 'tester@yahoo.com';

		//Gia tri bien de load qua view
		$data = array(
			'title'		=> $title,
			'address'	=> $address,
			'phone'		=> $phone,
			'email'		=> $email,
			'mylatlng'	=> $my_latlng
		);

		//Load file view
		$this->load->view('create_map/index', $data);

	}

}

/* End of file create_map.php */
/* Location: ./application/controllers/create_map.php */